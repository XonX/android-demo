package com.example.kalarel.androiddemo;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.kalarel.androiddemo.model.Message;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    @BindView(R.id.counter_text) TextView textView;
    @BindView(R.id.add_one) Button addOneButton;
    @BindView(R.id.message_text) TextView messageText;

    int counter = 0;

    public MainActivityFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, rootView);

        textView.setText(String.valueOf(counter));

        return rootView;
    }

    @OnClick(R.id.add_one) void addOne() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://jsonplaceholder.typicode.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        TestJson testJson = retrofit.create(TestJson.class);

        testJson.getMessage().enqueue(new Callback<Message>() {
            @Override
            public void onResponse(Call<Message> call, Response<Message> response) {
                messageText.setText(response.body().getTitle());
            }

            @Override
            public void onFailure(Call<Message> call, Throwable t) {

            }
        });

        textView.setText(String.valueOf(counter));
    }
}
