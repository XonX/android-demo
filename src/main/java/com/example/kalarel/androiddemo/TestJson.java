package com.example.kalarel.androiddemo;

import com.example.kalarel.androiddemo.model.Message;

import retrofit2.Call;
import retrofit2.http.GET;


public interface TestJson {

    @GET("/posts/1")
    Call<Message> getMessage();
}
