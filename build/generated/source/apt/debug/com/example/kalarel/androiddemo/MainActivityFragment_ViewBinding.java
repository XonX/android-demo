// Generated code from Butter Knife. Do not modify!
package com.example.kalarel.androiddemo;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MainActivityFragment_ViewBinding implements Unbinder {
  private MainActivityFragment target;

  private View view2131558538;

  @UiThread
  public MainActivityFragment_ViewBinding(final MainActivityFragment target, View source) {
    this.target = target;

    View view;
    target.textView = Utils.findRequiredViewAsType(source, R.id.counter_text, "field 'textView'", TextView.class);
    view = Utils.findRequiredView(source, R.id.add_one, "field 'addOneButton' and method 'addOne'");
    target.addOneButton = Utils.castView(view, R.id.add_one, "field 'addOneButton'", Button.class);
    view2131558538 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.addOne();
      }
    });
    target.messageText = Utils.findRequiredViewAsType(source, R.id.message_text, "field 'messageText'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    MainActivityFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.textView = null;
    target.addOneButton = null;
    target.messageText = null;

    view2131558538.setOnClickListener(null);
    view2131558538 = null;
  }
}
